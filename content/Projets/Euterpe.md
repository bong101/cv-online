+++
title = "Euterpe"
date = 1995-04-01

[taxonomies]
categories = ["DIY"]
tags = ["python", "raspberry"]
[extra]
toc = true
comments = false
+++

Fille de Zeus et de Mnémosyne, [Euterpe <i class="fa-brands fa-wikipedia-w fa-beat"></i>](https://fr.wikipedia.org/wiki/Euterpe) est la muse de l'Antiquité grecque qui s'amusait de la musique.

Le projet Euterpe est une espèce d'enceinte connectée, en DIY

C'est un appareil qui diffuse
- des webradios
- des MP3
- des Podcasts

# Schema technique
![](/images/Euterpe.png)

# Architecture matérielle

Euterpe est composée de : 

- [**Raspberry Pi 3**](https://www.raspberrypi.com/products/raspberry-pi-3-model-b-plus/) pour le coeur du système
- un hat [**Hifiberry Amp**](https://www.hifiberry.com/shop/boards/hifiberry-amp2/) pour amplifier le volume sonore
- un haut parleur type enceinte de voiture 
- un [**Arduino Uno**](https://store.arduino.cc/products/arduino-nano) pour la gestion du volume
- un écran epaper **WaveShare** pour un affichage en façade
- des boutons poussoirs, une led
- un **SSD** de 256 Go

Le tout est assemblé dans une boîte en contreplaqué.

## Architecture logicielle

La distribution qui tourne sur le Raspberry Pi est [**Dietpi**](https://dietpi.com/)
Le coeur logiciel d'Euterpe est basé sur [**Music Player Daemon**](https://www.musicpd.org/), MPD en abrégé, disponible sous toutes les bonnes distributions **Linux**. Pour interfacer les éléments matériels et MDP, j'ai conçu un ensemble de programmes **Python**

- Euterpe-core : le chef d'orchestre. Communique avec MPD, les différents matériels au travers des autres programmes python. Gére les différents Threads.
- Euterpe-volume est chargé de scruter le port usb, auquel est connecté un **Arduino**. Communique avec Euterpe-core via un socket.Unix
- Euterpe-gpio est chargé de scruter les GPIO, auxquels sont connectés les boutons MODE, Play/Pause, Next, Prev. Communique avec Euterpe-core via ZMQ
- Euterpe-ihm : Une interface web avec le microframework Flask, qui remplace toutes les fonctions matérielles et même plus : 
	- Gestion des Podcasts : en rentrant l'Url RSS d'un Podcasts dans un formulaire, Euterpe y souscrit. Les nouveaux podcasts seront systématiquement téléchargés.
	- Lors de la lecture des MP3, j'utilise l'API de [**Deezer**](https://api.deezer.com/) pour afficher les vignettes des artistes/groupes
	- Pour agrémenter les pages HTML, j'utilise le framework [**Bootstrap**](https://getbootstrap.com/)
	- Lecture d'un album, un groupe, un titre à la demande (ce qui n'est pas possible avec juste les boutons, ma MP3theque se composant de près de 8000 fichiers)

Ces programmes **Python** sont autant de services

## Captures d'écran	

{% galleria() %}
{
  "images": [
    {
      "src": "/images/Euterpe_MP3.png",
      "title": "Euterpe - MP3",
      "description": "Mode Lecture des MP3"
    },
    {
      "src": "/images/Euterpe_Radio.png",
      "title": "Euterpe - Radio",
      "description": "Mode Radio"
    },
    {
      "src": "/images/Euterpe_Podcasts.png",
      "title": "Euterpe - Page des Podcasts",
      "description": "Gestion des Podcasts."
    },
 {
      "src": "/images/Euterpe_Podcasts2.png",
      "title": "Euterpe - Page des Podcasts",
      "description": "Saisie d'un nouvel abonnement"
    }
    
  ]
}
{% end %}

## Photos

## To Do
- nettoyer le code
- changer la boite en contreplaqué par une caisse de magnum de vin ?
- améliorer l'IHM
