+++
title = "Ma Petite Gestion Immobiliere"
date = 1995-04-01

[taxonomies]
categories = ["DIY"]
tags = ["python", ]
[extra]
toc = true
comments = false
+++



Le projet est venu d'une de mes connaissances qui avait du mal à faire les quittances papier pour sa location en temps et en heures.
J'ai donc imaginé une interface pour le faire facilement. Envoyer une quittance prend donc moins de 3 minutes, consultation bancaire comprise.

le workflow de départ était le suivant :

Verification des paiements -> Saisie des paiements dans l'interface -> Génération du PDF -> Envoi par mail 

Les composants logiciels sont :

- Python
avec les bibliothèques suivantes :
	- SQLAlchemy (ORM)
	- Flask, le microframework Web
- MYSQL ou 
- SQLite3
- HTML
	- framework Boostrap


Actuellement, les fonctionnalités sont les suivantes :

- Liste Ajout Modification de biens immobiliers
- Liste Ajout Modification des locations
- Liste Ajout Modification des encaissements
- Génération des quittances
- Visualisation des quittances
- Envoi par mail des quittances
- Liste Ajout Modification des dépenses

## Captures d'écran	

{% galleria() %}
{
  "images": [
    {
      "src": "/images/ma-PGI_1.png",
      "title": "Accueil",
      "description": "Ma PGI - Accueil"
    },
    {
      "src": "/images/ma-PGI_2.png",
      "title": "Pages des appartements",
      "description": "Ma PGI - Appartements"
    },
    {
      "src": "/images/ma-PGI_3.png",
      "title": "Pages des encaissements",
      "description": "Ma PGI - Encaissements."
    },
 {
      "src": "/images/ma-PGI_4.png",
      "title": "Saisie d'une dépense",
      "description": "Ma PGI - Formulaire saisie dépenses"
    },
{
      "src": "/images/ma-PGI_5.png",
      "title": "Modification d'un encaissement",
      "description": "Ma PGI - Formulaire saisie encaissement"
    },{
      "src": "/images/ma-PGI_6.png",
      "title": "Quittances",
      "description": "Ma PGI - Quittances"
    }
    
  ]
}
{% end %}


# To Do
- Passage avec un backend API. J'ai developpé une API générique (Ajout, modification, suppression, liste, tout en JSON), que je dois décliner pour tous les objets de cette application.
- Installation dans Docker, pour faire des tests
- Balance comptable annuelle
- Un agenda, pour gérer des tâches récurrentes
	- Les quittances
	- Les réunions de copropriétaires
	- Les impôts (revenus et fonciers)





