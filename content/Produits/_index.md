+++
title = "Produits et Outils"

description = "Petite liste plus ou moins exhaustive de solutions que j'ai pu rencontrer, soit dans ma vie personnelle, soit dans ma vie professionnelle. Sur une échelle de 1 à 5, mes connaissances vont de 2 à 4."



+++
#
#

### Langages de programmation : 

- Python 
  - [x] Pip 
  - [x] Alembic 
  - [x] Flask 
  - [x] Pytest
  - [x] Selenium
	
- Bash, Ksh
- Perl 

### Bases de données : 

- MySQL 
- Oracle 
- Postgres 
- SQLite3 

### Serveurs d'applications Java : 

- Tomcat 
- Oracle Weblogic 
- Jboss 

### Ordonnanceurs : 

- Control+M 
- Rundeck 
- CA7 (z/OS) 
- TWS (z/OS) 

### Supervision : 

- Nagios 


### Outils : 

- Git 
- Gitlab 
- Jenkins 
- Ansible 

### Virtualisation : 

- VmWare
- Docker
