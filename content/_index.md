+++
title = "CV en ligne Olivier GRUEL"

description = "Mon CV n'avait pas été relooké depuis ...très très longtemps. J'ai décidé de partir sur autre chose que ce fichu fichier LibreOffice"
+++

Je m'appelle **Olivier GRUEL**, j'ai 52 ans, un enfant, et je suis informaticien.


Vous pouvez découvrir ici [les formations suivies](/Formations), [mon parcours professionnel](/Experiences), [les logiciels](/Produits),  [mes compétences](/Competences) et [les projets personnels](/Projets) auxquels je consacre une partie de mon temps libre

Je suis aussi passionné par : 

# Intérêts

- DIY
- Logiciels Opensource
- Randonnées en moyenne montagne
- Stephen King
- Jardinage


Amis recruteurs, l'ancien [CV <i class="fa-regular fa-file-pdf fa-beat"></i>](/pdf/CV_Olivier_GRUEL_2023.pdf) est toujours là!