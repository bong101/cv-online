+++
title = "Formations Professionnelles"
date = 1995-04-01

[taxonomies]
categories = ["Formations"]
tags = ["formations"]
[extra]
toc = true
comments = false
+++

Aves mes différents employeurs, j'ai pû suivre les formations suivantes : 

## Cap Gemini - Sogeti

- Microsoft Windows 2000 (2001)
- AS/400 (2002)
- Lotus Notes (2002)
- Citrix (2002)
- Unix (2008)
- IBM Websphere (2008)
- Vmware (2008)
- $Universe (2008)
- Cursus complet VmWare 4.1 (2011)

## Atos

- Oracle (2012)
- Apache (2014)
- Tivoli Storage Management (2015)
- Apache Hadoop (2016)

## Neosoft

- Ansible (2021) 
- Docker (2021)
