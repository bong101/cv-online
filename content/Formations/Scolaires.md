+++
title = "Parcours  scolaire"
date = 1995-04-01

[taxonomies]
categories = ["Formations"]
tags = ["formation"]
[extra]
toc = true
comments = false
+++

- Bac F3 Électrotechnique Mention Bien (1990)
- BTS Informatique Industrielle (1992)