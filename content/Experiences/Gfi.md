+++
title = "GFI"
date = 2017-01-01
updated = 2023-07-01
[taxonomies]
categories = ["Employeur"]
tags = ["service", "Ingénieur de Production", "Assistant MOA"]
[extra]
toc = true
comments = false
+++

![](/images/GFI_Logo.jpg)

Nouvel employeur, nouveaux challenges ! Dire qu'à cette occasion, j'étais collègue avec un certain Kilian M. et Nikolas K.....
### Période 
Janvier 2017 - Mai 2019


# Argic-Arcco

![](/images/Agirc_Arcco_Logo.jpg)
Toujours dans le domaine applicatif, je deviens **Ingénieur de Production** pour le GIE **Argic-Arcco**, avec notamment les applications **RNE/RNA/DSN**. 
### Poste occupé
Ingénieur de production
### Lieu
Orléans
### Période
Juillet 2017 - Août 2018 

### Tâches
- Préparation des plans batchs, reprise des incidents, production météo flux et batchs
- Coordination des mises en production.
- Passage de jobs à la demande des équipes fonctionnement courant
- Surveillance des servitudes
- Créer/Ordonnancer des applications, des jobs et des dépendances pour les opérations exceptionnelles
- Traiter les incidents de 1er niveau relatif aux bases de données relevant du périmètre de responsabilité exploitation

___
# Orange
![](/images/Orange_Logo.png)

### Poste occupé
Assistant MOE
### Lieu
Orléans
### Période
Août 2018 - Avril 2019
### Tâches
- Migration de flux CFT
- Sécurisation des flux CFT et Webservices avec la PKI entreprise
- Anonymisation des données plateformes hors-production
- Installation des plateformes DEV et Qualif (Apache, PHP, MYSQL)
### Ce que j'ai aimé
La PKI en "libre-service" pour les environnements bas, pour sécuriser les applications dont j'étais responsable