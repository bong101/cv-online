+++
title = "Cap Gemini - Sogeti"
date = 1998-05-01
updated = 2023-09-01

[taxonomies]
categories = ["Employeur"]
tags = ["service", "mission", "administrateur systeme", "applicatif"]
[extra]
toc = true
comments = false
+++

![](/images/Sogeti_Logo.jpg)

<p style='text-align: justify;'>Désireux de donner plus de "sérieux" à mon parcours professionnel, j'ai rejoint un grand groupe français de services en Ingénierie Informatique, dans une agence "Centre et Services" (ce sera important pour la suite). Faisant partie des divisions exploitation de Cap Gemini, nous allons être transféré à Sogeti, pour une renaissance de cette marque fondatrice. </p>

### Période 
Mai 1998 - Juillet 2011
  
# Cap Gemini (Division Tertiaire)
![](/images/Cap_Gemini_Logo.png)

Pour ma première mission, j'ai intégré une autre division du groupe, à Paris La Défense, pour apporter une assistance informatique. Ma premiere expérience avec du Linux <i class="fa-brands fa-linux fa-beat"></i> dans le monde professionnel.
### Poste occupé
Administrateur Systèmes
### Lieu 
Paris La Défense
### Période 
Mai 1998 - Décembre 2001
### Tâches
    
- Gestion d’un parc informatique (200 postes, 250 portables et une dizaine de serveurs)
- Administrateur réseau et messagerie
- Responsable des achats de matériel (machines et licences de logiciels)
- Assistance et support niveau II

### Ce que j'ai aimé
Pouvoir mettre en pratique mes connaissances réseau acquises en autoformation et en TP du soir
### Actions - Projets que j'ai envie de mettre en avant : 
La plage d'adresse IP réservée à nos bureaux étant régulièrement saturée et l'équipement central (powerhub) ne pouvant être reconfiguré, j'ai installé un serveur DHCP sous Linux qui répondait plus vite que le powerhub, avec des baux beaucoup plus petits!
___
# Banque Hervet
![](/images/Banque_Hervet_Logo.png)

Première mission en région Centre, la **Banque Hervet** (aujourd'hui filiale de HSBC ?)! Je vous avais dit que le nom de l'agence était important!!
### Poste occupé
Administrateur Systèmes
### Lieu 
Fussy, près de Bourges (18)
### Période 
Année 2001
### Tâches : 
- Réalisation de projets (déploiement SP6, réalisation d’état de suivi de parc en Perl...)
- Assistance et support niveau II
- Suivi quotidien de certains paramètres du parc (antivirus, espace disque…)

### Ce que j'ai aimé : 
m'autoformer au langage de programmation Perl


___
# Paragon
![](/images/Paragon_Logo.png)
Ensuite, nouvelle mission en région Centre, au sein d'une imprimerie, **Paragon** qui avait privilégié les investissements sur la production ( rotatives, etc) au détriment de son informatique. Le défi était grand. Belle mission qui aura duré 3 ans
### Poste occupé
Administrateur Systèmes, Responsable d’un service informatique en infogérance
### Lieu : Cosne sur Loire (58)
Avec des déplacements réguliers à Argent sur Sauldre et Romorantin, et des déplacements ponctuels à Paris, Nantes, Lyon et Gand
### Période : 2002 - 2005
### Tâches : 
- Responsable d'une équipe de 3 personnes
- Gestion du parc informatique, administrateur système et réseau
- Participation active à un plan d’amélioration du SI du client (upgrade serveur, réseau, ...)
- Mise en place de solutions techniques (clonage de poste avec Ghost, installation sauvegarde, installation serveurs de mise à jour Windows, etc..)
- Rédaction de procédures

### Ce que j'ai aimé : 
- Proposer des solutions à moindre coût pour résorber le retard accumulé pendant pas mal d'années
- que le client me choisisse pour assurer la responsabilité de la mission (après deux essais infructueux)
### Ce que j'ai moins aimé : 
Le renouvellement du contrat par le client, pour le dénoncer un mois après, avec cette simple consigne : "Soyez inventifs !". Contrat perdu, donc.
### Actions - Projets que j'ai envie de mettre en avant : 
Tout le travail éffectué lors de cette période !
___        
#  EDF/GDF
![](/images/Edf_Logo.png)

Nouveau challenge : une mission de **coordination**, pour occuper un poste de Correspondant Serveur Application pour **EDF**. Une interface entre les chefs de projets **GDF** et l'infogérant, **Atos**. Il sera à nouveau question d'[**Atos**](/Experiences/atos) plus tard.
### Poste occupé 
Correspondant Serveur - Application
### Lieu
Orléans
### Période
Mai 2005 - novembre 2007
### Tâches
- Suivi de l’activité et de la facturation des infogérants
- Coordination et planification de toutes les actions techniques sur les projets ou les applications confiées 
- Organisation et animation des réunions avec les clients
- Définition avec les pilotes opérationnels d’applications des rapports d’exploitation à fournir, en afficher les coûts de production, s’assurer de la mise en place et de la production à la périodicité convenue

### Ce que j'ai aimé
Allez voir les Pilotes d'Applications pour parler des nouveaux projets
### Actions - Projets que j'ai envie de mettre en avant
Une application GED avait été mal paramétrée au niveau des disques, il a fallu discuter avec tous les acteurs pour refaire la configuration, notamment parce que la volumétrie était importante et le gestionnaire de disques inapproprié pour cette volumétrie 
___
#  SNCF
![](/images/SNCF_Logo.png)	
### Poste occupé 
Intégrateur applicatif
### Lieu
Paris
### Période
Novembre 2008 - Mars 2009
### Tâches
- Maintien en opérationnel des plateformes d’Intégration, de Recette Utilisateur et de Pré-production
- Intégration des nouvelles versions des applications Unix en respectant les processus ITIL, puis installation sur les plateformes de RU et PP
- Suivi des flux, des chaînes batch, les imports/exports de données


___
#  Total
![>](/images/Total_Logo.jpeg)		

Dernière mission avec Cap Gemini, un gros projet de déploiement mondial de Windows Serveur 2008/Windows 7 pour la société **Total** 
### Poste occupé 
Administrateur Systèmes
### Lieu
Paris La Défense
### Période
Avril 2009 – Juillet 2011
### Tâches
- Déployer de nouveaux serveurs
- Administration de la nouvelle plate-forme (AD et Exchange)
- Résolution d’incidents niveau II
- Migration des utilisateurs  
___
