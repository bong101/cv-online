+++
title = "NeoSoft"
date = 2019-05-01
updated = 2023-06-01
[taxonomies]
categories = ["Employeur"]
tags = ["service", "Analyste Technique", "Ingénieur DevOps"]
[extra]
toc = true
comments = false
+++
![](/images/Neo-soft_Logo.jpg)

Enchanté par une équipe bienveillante, je rejoins NeoSoft, ESN avec un avenir prometteur

### Période
Mai 2019 - toujours en cours
___

# Accor Hotels
![](/images/Accor_Logo.png)

Pour bien démarrer avec NeoSoft, je fais jouer mon réseau et je trouve cette mission d'Analyste Technique Applicatif auprès de mon ancien employeur, [Atos](/Experiences/atos)
### Poste occupé
Analyste Technique
### Lieu
Orléans
### Période
Mai 2019 - Décembre 2020
### Tâches
- Résolution des incidents applicatifs niveau II & III
- Déploiement en production des applications
- Rédactions des Dossiers d’Exploitation des Applications
- Astreintes Système Unix
- Lancement des batchs Talend

### Ce que j'ai aimé
Faire les astreintes systèmes Unix/Linux
### Ce que j'ai moins aimé
Un peu trop de copier-coller à mon goût


___

# Rectorat du Loiret
![](/images/AC_Orleans_Tours_Logo.jpeg)

L'opportunité se présente d'intégrer une nouvelle équipe d'intégration dédiée DevOps, opportunité que je saisis immédiatement
### Poste occupé
Ingénieur DevOps
### Lieu
Orléans
### Période
Décembre 2020 - Juin 2023
### Tâches
- Création de playbooks Ansible, avec tests intégrés
- Lancement de ces playbooks (via Jenkins ou AWX)
- Rédaction des procédures à destination de l'exploitation
- Création d'environnements à la volée dans le cloud avec Terraform & Ansible
- Suivi de l'évolution des applications
- Suivi des montées de versions des briques applicatives (réduction de la dette technique)
- Suivi des incidents
- Pilotage de l'application LIEN (Logiciel Infirmier de l'EN)
### Ce que j'ai aimé
Découverte de nouveaux produits (Ansible, Docker, Jenkins..)

### Actions - Projets que j'ai envie de mettre en avant


___

# COVEA - GMF

![](/images/Covea_Logo.jpg)

### Poste occupé

### Lieu
Orléans
### Période
Octobre 2023 - Toujours en cours
### Tâches
- Suivi, de la coordination et planification batch.
- Planification et suivi des plans d’exploitation batch dans les différents environnements à travers les outils d’exploitation.
- Analyse des incidents de niveau 1
- Communication et Suivi des incidents.
- Coordination opérationnelle des interventions des mises en production.
- Analyse et optimisation des plans.
