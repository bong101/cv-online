+++
title = "Atos"
date = 2011-07-01
updated = 2023-08-01
[taxonomies]
categories = ["Employeur"]
tags = ["service", "intégrateur","applicatif","analyste technique"]
[extra]
toc = true
comments = false
+++

![](/images/Atos_Logo.png)	

J'ai rejoint la société **Atos**, pour me "poser", effectuer moins de déplacements, notamment.

 

### Période 
Juillet 2011 - Janvier 2017

# RTE
![](/images/RTE_Logo.png)	

Pour **RTE**, je deviens **Intégrateur applicatif**. La meilleure mission de toute ma vie. Et la plus longue aussi. Et la meilleure.

### Poste occupé
Intégrateur applicatif
### Lieu
Orléans
### Période
Juillet 2011 - Novembre 2015
### Tâches
- Participer au lancement des projets en liaison avec les différents acteurs
- Être l’interlocuteur privilégié entre les projets et les équipes techniques d’exploitation
- Participer aux travaux de qualification et validation technique des livraisons
- Être garant du respect des normes et procédures de livraisons pour la production
- Valider l’exploitabilité des applications, rédiger les rapports de MEP Serveurs
- Déployer en production les socles techniques (serveurs) et applications préalablement qualifiées
- Responsable d'un cluster Vmware ESX hébergeant les Vms de l'environnement d'Integration
- Responsable des bascules applicatives inter-sites
### Ce que j'ai aimé
Comprendre comment fonctionnaient vraiment les applications, les middleware. Voir les différentes architectures applicatives possibles

### Actions - Projets que j'ai envie de mettre en avant
- Proposer un wiki d'équipe à mes collègues sous Wordpress
- Coordonner les bascules applicatives intersites à froid 

___

# Analyste technique Applicatif pour SNCF Réseau
![](/images/SNCF_Réseau_Logo.png)

**SNCF Réseau** (anciennement RFF) était un nouveau contrat d'infogérance gagné par Atos, j'ai décidé de rejoindre le Bureau Technique Applicatif. Mes premiers pas en **Python**<img src="https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/python.svg" width="25" height="25">

### Poste occupé

### Lieu
Orléans
### Période
Novembre 2015 - Janvier 2017
### Tâches
- Résolution des incidents applicatifs niveau II & III
- Gestion des chaînes d’ordonnancement VTOM (Pilotage)
- Création d’un robot de supervision à base de scripts Selenium en Python
### Ce que j'ai aimé
Faire les astreintes applicatives

### Action que j'ai envie de mettre en avant :
La météo du matin se faisait manuellement ... Il fallait se connecter sur les différentes IHM des applications, ce qui prenait un temps fou. J'ai donc créé un programme **Python**, avec **Selenium** qui faisait ce travail en automatique.

___
