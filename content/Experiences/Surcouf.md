+++
title = "Surcouf"
date = 1995-04-01
updated = 2023-10-01
[taxonomies]
categories = ["Employeur"]
tags = ["service", "technicien"]
[extra]
toc = true
comments = false
+++


![](/images/Surcouf_Logo.jpg)

<p style='text-align: justify;'>Au sortir de deux ans de service national, j'ai rejoint cette enseigne de la distribution de composants informatiques, à l'esprit de start-up. L'arrivée des nouvelles technologies (Microsoft Windows 95, Intel Pentium, etc) a rendu cette expérience très agréable.</p>

### Poste occupé
Technicien Informatique au stand montage
### Lieu 
Paris, Avenue Daumesnil
### Période 
Avril 1995 - Mai 1998
### Tâches

 - Préparation et dépannage des PC,
 - Formation des utilisateurs à tous les nouveaux produits,
 - Accueil, dépannage, conseil et installation de matériel sur plus de 3000 PC. 


![](/static/Surcouf_Logo.jpg)